use atty::Stream;
use clap;
use std::io::{self, Read};
mod expander;

use expander::expand;

fn main() {
    let args = generate_parser().get_matches();
    print!("{}", expand_text(&args, atty::is(Stream::Stdout)));
}

fn generate_parser<'a, 'b>() -> clap::App<'a, 'b> {
    clap::App::new("Expando")
        .version("0.0.2")
        .author("Darrien Glasser <me@darrien.dev>")
        .about("Expands text sometimes idk")
        .setting(clap::AppSettings::TrailingVarArg)
        .arg(
            clap::Arg::with_name("EXPAND_TEXT")
                .required(false)
                .takes_value(true)
                .multiple(true)
                .help("Text to expand"),
        )
        .arg(
            clap::Arg::with_name("ALL_CAPS")
                .required(false)
                .short("a")
                .long("all-caps")
                .help("Make expanded text all caps"),
        )
        .arg(
            clap::Arg::with_name("SPONGE")
                .required(false)
                .conflicts_with("ALL_CAPS")
                .short("s")
                .long("sponge")
                .help("MaKe TExt SpONgElIkE"),
        )
}

fn expand_text(input: &clap::ArgMatches, istty: bool) -> String {
    let mut buffer = String::new();
    let user_text: Vec<_> = if input.is_present("EXPAND_TEXT") {
        input.values_of("EXPAND_TEXT").unwrap().collect()
    } else {
        io::stdin().read_to_string(&mut buffer).unwrap();
        vec![&*buffer]
    };
    let all_caps = input.is_present("ALL_CAPS");
    let sponge = input.is_present("SPONGE");

    let result = expand(user_text, sponge, all_caps);
    let new_line = if !istty { "" } else { "\n" };
    format!("{}{}", result, new_line)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn expand_cli_works() {
        let parser = generate_parser();
        let args = parser.get_matches_from(vec!["expando", "bubber"]);
        let result = expand_text(&args, false);
        assert_eq!(result, "b u b b e r");
    }
    #[test]
    fn caps_cli_works() {
        let parser = generate_parser();
        let args = parser.get_matches_from(vec!["expando", "-a", "bubber"]);
        let result = expand_text(&args, false);
        assert_eq!(result, "B U B B E R");
    }
    #[test]
    fn expand_text_newline_works() {
        let parser = generate_parser();
        let args = parser.get_matches_from(vec!["expando", "bubbers", "are", "bubs"]);
        let mut result = expand_text(&args, false);
        assert_eq!(result, "b u b b e r s   a r e   b u b s");
        result = expand_text(&args, true);
        assert_eq!(result, "b u b b e r s   a r e   b u b s\n");
    }
}
