CC=cargo
CFLAGS=--release
BIN=expando
BIN_PATH=target/release

all: release mac

release:
	$(CC) build $(CFLAGS)
	strip $(BIN_PATH)/$(BIN)

check:
	cargo clippy --all -- -D warnings
	cargo test
	cargo fmt -- --check

fmt:
	cargo fmt
