# expando - The text expander cli

## What?

```bash
~ expando -a there comes a time when the operation of the machine becomes...
 T H E R E   C O M E S   A   T I M E   W H E N   T H E   O P E R A T I O N   O F   T H E   M A C H I N E   B E C O M E S . . .
```

Sometimes you need a little fun, and expando is there to make your messages,
more, uh, fun I guess.

Want to expand your text from the CLI? Expando's got you. Upper or lowercase,
it's there and FAST because it's rust I guess.

Just run `expando some text` and expando will expand it. Run with `-a` and it'll
 C A P I T A L I Z E. Yes I did use `expando` to type that because it's way
easier.

thERe'S ALSo A fun sPONge mode bEcAUse EVEN tHougH ThE MemE Is deAD I waS bOrEd.

Run with `-s` to activate sponge mode.

## Why?

idk

Click on releases if you're bored as hell and want a binary for Linux.
